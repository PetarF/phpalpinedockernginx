# PHPAlpineDockerNginx

Development environment suitable for PHP projects most common extensions installed out of the box

## Getting started

```
make up // start project/build containers
make down // drop containers
make enter {container} // enter container in bash
make sh {container} // enter container in shell
make run {command} // execute a command in a new container
make exec {command} // execute a command in running container
make sh //docker compose ps --all
```

