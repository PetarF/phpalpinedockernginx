#!/usr/bin/env sh
echo "Creating prod, dev and test databases"
mysql -u root -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE \`${MYSQL_DATABASE}_test\`;"