#!/usr/bin/env sh
echo "Granting all privileges to user ${MYSQL_USER} ON ${MYSQL_DATABASE} AND ${MYSQL_DATABASE}_% databases"
mysql -u root -p${MYSQL_ROOT_PASSWORD} -e "GRANT ALL PRIVILEGES ON \`${MYSQL_DATABASE}\\_%\`.* TO \`${MYSQL_USER}\`@\`%\`;"
mysql -u root -p${MYSQL_ROOT_PASSWORD} -e "GRANT ALL PRIVILEGES ON \`${MYSQL_DATABASE}\`.* TO \`${MYSQL_USER}\`@\`%\`;"