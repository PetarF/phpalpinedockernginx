#!/usr/bin/env sh
cd /opt/project/;
DIRECTORY=./node_modules
if [ -d "$DIRECTORY" ]; then
    yarn encore dev --watch;
else
    yarn install;
	yarn encore dev --watch;
fi