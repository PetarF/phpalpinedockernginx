FROM php:8.2-fpm-alpine AS php_base

WORKDIR /opt/project

ARG HOST_USER_ID
ARG HOST_GROUP_ID

RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted gnu-libiconv
#  Install dependancies
RUN apk add --no-cache $PHPIZE_DEPS \
        bash \
        # Dependancy for git
		git \
		# Dependancy for zip/unzip
		unzip \
		libzip-dev \
		# Dependancy for timezone
		tzdata \
		# Dependancy for intl
		icu-libs \
        libintl \
        icu \
        icu-dev \
        # Dependancy for GD
        freetype \
        freetype-dev \
        libjpeg-turbo \
        libjpeg-turbo-dev \
        libpng \
        libpng-dev \
        # XML
        php82-dom \
        libxml2-dev \
        libxml2

# Timezone
ENV TZ Europe/Zagreb
RUN echo "date.timezone = "$TZ >> /usr/local/etc/php/conf.d/timezone.ini
		
# Composer
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    composer global config bin-dir /usr/local/bin

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
COPY ./php.ini /usr/local/etc/php/php.ini

# Install php extensions
RUN echo "Install php extensions" && \
    # Intl
    docker-php-ext-configure intl --enable-intl && docker-php-ext-install intl && \
    # PDO
    docker-php-ext-install pdo_mysql && \
    # Zip
    docker-php-ext-install zip && \
    # Dom
    docker-php-ext-install dom && \
    # Xml
    docker-php-ext-install xml

## GD library
RUN apk add --no-cache libpng libpng-dev libjpeg-turbo-dev libwebp-dev zlib-dev libxpm-dev && \
    docker-php-ext-install gd && \
    apk del libpng-dev && apk del libjpeg-turbo-dev && apk del zlib-dev && apk del libxpm-dev

# Add xdebug
RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS && \
    apk add --update linux-headers && \
    pecl install xdebug-3.2.0 && \
    docker-php-ext-enable xdebug && \
    apk del -f .build-deps

# Configure Xdebug
RUN echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.log=/var/www/html/xdebug/xdebug.log" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.discover_client_host=1" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.client_port=9000" >> /usr/local/etc/php/conf.d/xdebug.ini \

## OPCACHE
RUN docker-php-ext-install opcache && docker-php-ext-enable opcache \
	&& echo "opcache.max_accelerated_files = 20000" >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
	
##SYMFONY CONSOLE SHORTCUT
RUN ln -s /opt/project/bin/console /usr/local/bin/c

## Amqp
#RUN apk add --no-cache rabbitmq-c-dev rabbitmq-c $PHPIZE_DEPS && \
#    pecl install amqp && \
#    apk del rabbitmq-c-dev $PHPIZE_DEPS && \
#    docker-php-ext-enable amqp

## Php tools
#RUN  composer global require friendsofphp/php-cs-fixer

COPY ./conf.d/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
## Start script
COPY ./start.sh /opt/scripts/start.sh
COPY ./xdebug.sh /opt/scripts/xdebug.sh

ENV USER=www-data
ENV UID=$HOST_USER_ID
ENV GID=$HOST_GROUP_ID

RUN deluser --remove-home $USER
#RUN groupdel $USER
RUN addgroup -g $GID $USER
RUN adduser -u $UID -G $USER -h /home/$USER -D $USER

#RUN mkdir /etc/sudoers.d/$USER &&echo "$USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER \ && chmod 0440 /etc/sudoers.d/$USER

RUN chmod a+x /opt/scripts/start.sh
RUN ln -s /opt/scripts/xdebug.sh /usr/local/bin/start-xdebug
RUN chown -R www-data:www-data /opt/project
USER www-data

CMD ["/opt/scripts/start.sh"]

#FROM php_base AS php_supervisord

## Supervisord
#RUN apk add --update supervisor && rm  -rf /tmp/* /var/cache/apk/*
#ADD ./supervisor.conf /etc/supervisor.conf
#ADD ./messenger-worker.conf /etc/supervisor/conf.d/messenger-worker.conf