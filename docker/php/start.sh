#!/usr/bin/env sh

if [ ! -f /opt/chmod.txt ] 
then
	touch /opt/chmod.txt
  chown -R www-data:www-data /opt/project
	find /opt/project -type d -exec chmod 755 {} \;
	find /opt/project -type f -exec chmod 644 {} \;
fi 

chmod a+x /opt/project/bin/console
chmod a+x /opt/scripts/xdebug.sh

/usr/local/sbin/php-fpm