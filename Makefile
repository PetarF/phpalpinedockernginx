## Configuration start ##
.EXPORT_ALL_VARIABLES:
COMPOSE_FILE=docker-compose.yml
COMPOSE_DOCKER_CLI_BUILD=1
DOCKER_BUILDKIT=1
## Configuration end ##

RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
# ...and turn them into do-nothing targets
$(eval $(RUN_ARGS):;@:)
SHELL = /bin/sh

DOCKER_HOST_USER_ID=$(shell id -u)
DOCKER_HOST_GROUP_ID=$(shell id -g)

export DOCKER_HOST_USER_ID
export DOCKER_HOST_GROUP_ID

ifneq ("$(wildcard /usr/local/bin/docker-compose) || $(wildcard /usr/bin/docker-compose)","")
    DOCKER_COMPOSE = docker-compose
else
    DOCKER_COMPOSE = docker compose
endif

$(info DOCKER_HOST_USER_ID: $(DOCKER_HOST_USER_ID))
$(info DOCKER_HOST_GROUP_ID: $(DOCKER_HOST_GROUP_ID))
$(info DOCKER_COMPOSE: $(DOCKER_COMPOSE))

help:
	echo "Available commands are up, down, clean, run, exec, sh, enter, ps, phpstan, phpcs, phpcsfix, rector"
## Commands ##
# Builds, (re)creates, starts, and attaches to containers for a service. in the background
up:
	$(if $(strip $(RUN_ARGS)),$(DOCKER_COMPOSE) up $(RUN_ARGS),$(DOCKER_COMPOSE) up --build -d)
# Stops containers and removes containers, networks, volumes, and images created by up.
down:
	$(DOCKER_COMPOSE) down $(RUN_ARGS)
# Cleanup images and containers related to this project
clean:
	$(DOCKER_COMPOSE) down --rmi all --volumes --remove-orphans
# docker-compose run command
run:
	$(DOCKER_COMPOSE) run $(RUN_ARGS)
# docker-compose exec command
exec:
	$(DOCKER_COMPOSE) exec $(RUN_ARGS)
# Sh into the project container
sh:
	$(DOCKER_COMPOSE) exec $(RUN_ARGS) sh
enter:
	$(DOCKER_COMPOSE) exec $(RUN_ARGS)
ps:
	$(DOCKER_COMPOSE) ps --all

#symfony project specific commands:

#run php stan code analysis
phpstan:
	$(DOCKER_COMPOSE) run php php vendor/bin/phpstan analyse --error-format symplify --memory-limit=-1
#run Easy Code Standard checks
phpcs:
	$(DOCKER_COMPOSE) run php php vendor/bin/ecs check
#run Easy Code Standard fixes
phpcsfix:
	$(DOCKER_COMPOSE) run php php vendor/bin/ecs check --fix
#run rector
rector:
	$(DOCKER_COMPOSE) run php php vendor/bin/rector process
#run symfony clear cache
cc:
	$(DOCKER_COMPOSE) run php bin/console c:c